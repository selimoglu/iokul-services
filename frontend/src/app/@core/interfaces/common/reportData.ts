
import { Observable } from 'rxjs';
export abstract class ReportData {
  schoolArr = [];
  shuttleArr = [];
  schoolData = [
    {
      schoolId: "",
      schoolName: "",
      schoolAddress: "",
      schoolLatitude: "",
      schoolLongitude: "",
    }
  ];

  shuttleData = [
    {
      shuttleId: "",
      shuttleName: "",
      shuttleCode: "",
      shuttlePhone: "",
      targetsIds: [],
      plate: "",
      mark: "",
      model: "",
      modelYear: "",
      capacity: "",
    }
  ];
  selecteds = {};
  studentData = [
    {
      address: "",
      studentCode: "",
      studentId: "",
      studentName: "",
      parentCode: "",
      parentName: "",
      parentPhone: "",
      shuttleName: "",
      shuttleId: "",
      targetName: "",
      targetId: "",
      studentLatitude: "",
      studentLongitude: "",
    }
  ];

  abstract getTargets(): Observable<any>;
  abstract createTarget(id, name, address, latitude, longitude): Observable<any>;
  abstract removeTarget(schoolId): Observable<any>
  abstract getShuttle(): Observable<any>;
  abstract createShuttle(id, name, code, phone, targetsId, plate, mark, model, modelYear, capacity): Observable<any>;
  abstract removeShuttle(shuttleId): Observable<any>
  abstract getStudents(): Observable<any>;
  abstract createStudent(address, studentCode, studentId, studentName, parentCode, parentName, parentPhone, shuttleId, targetId, latitude, longitude): Observable<any>;
  abstract removeStudent(studentId): Observable<any>
}

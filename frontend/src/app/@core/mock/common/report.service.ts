
import { Injectable } from '@angular/core';
import { ReportData } from '../../interfaces/common/reportData';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { of as observableOf, Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
@Injectable()
export class ReportService extends ReportData {

  constructor(private httpClient: HttpClient, private route: ActivatedRoute) {
    super();
  }
  endpoint = 'https://servis.iokul.com/api';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getTargets(): Observable<any> {
    return this.httpClient.get<any>(this.endpoint + '/shuttles/target',
      this.httpOptions).pipe();
  }

  createTarget(id, name, address,latitude,longitude): Observable<any> {
    const requestBody = {
      "adrLocation": {
        "address": address,
        "location": {
          "latitude": latitude,
          "longitude": longitude
        }
      },
      "id": id,
      "name": name
    };
    return this.httpClient.post<any>(this.endpoint + '/shuttles/create/target',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }

  removeTarget(schoolId): Observable<any> {
    return this.httpClient.post<any>(this.endpoint + '/shuttles/remove/target/' + schoolId,
      this.httpOptions).pipe();
  }

  getShuttle(): Observable<any> {
    return this.httpClient.get<any>(this.endpoint + '/shuttles',
      this.httpOptions).pipe();
  }

  createShuttle(id, name, code, phone, targetIds, plate, mark, model, modelYear, capacity): Observable<any> {
    const requestBody = {
      "car": {
        "capacity": capacity,
        "mark": mark,
        "model": model,
        "modelYear": modelYear,
        "plate": plate
      },
      "code": code,
      "id": id,
      "name": name,
      "phone": phone,
      "targetIds": targetIds
    };
    return this.httpClient.post<any>(this.endpoint + '/shuttles/create',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }

  removeShuttle(shuttleId): Observable<any> {
    return this.httpClient.post<any>(this.endpoint + '/shuttles/remove/' + shuttleId,
      this.httpOptions).pipe();
  }

  getStudents(): Observable<any> {
    return this.httpClient.get<any>(this.endpoint + '/shuttles/student',
      this.httpOptions).pipe();
  }

  createStudent(address, studentCode, studentId, studentName, parentCode, parentName, parentPhone, shuttleId, targetId,latitude,longitude): Observable<any> {
    const requestBody = {
      "adrLocation": {
        "address": address,
        "location": {
          "latitude": latitude,
          "longitude": longitude
        }
      },
      "code": studentCode,
      "id": studentId,
      "name": studentName,
      "parentCode": parentCode,
      "parentName": parentName,
      "parentPhone": parentPhone,
      "shuttleId": shuttleId,
      "targetId": targetId,
    };
    return this.httpClient.post<any>(this.endpoint + '/shuttles/create/student',
      JSON.stringify(requestBody),
      this.httpOptions).pipe();
  }

  removeStudent(studentId): Observable<any> {
    return this.httpClient.get<any>(this.endpoint + '/shuttles/remove/student/' + studentId,
      this.httpOptions).pipe();
  }

}

import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SchoolEditComponent } from '../school-edit/school-edit.component';
import { NbDialogService } from '@nebular/theme';
import { ReportData } from '../../../@core/interfaces/common/reportData';
import { SchoolNewComponent } from '../school-new/school-new.component';

@Component({
  selector: 'school-report-table',
  templateUrl: './school-report-table.component.html',
  styleUrls: ['./school-report-table.component.scss'],
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class SchoolReportTableComponent {
  settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      name: {
        title: 'Okul Adı',
        type: 'string',
      },
      address: {
        title: 'Adresi',
        type: 'string',
      },
      location: {
        title: 'Konumu',
        type: 'string',
      },
    },
  };
  source: LocalDataSource = new LocalDataSource();
  firstClickRow: string;
  id: string;
  sidebarState: boolean;
  code: string;
  controlClick: number;
  firstClick = 0;
  secondClick = 0;
  constructor(private reportService: ReportData, private dialogService: NbDialogService) {

  }

  onDeleteConfirm(event): void {

    if (window.confirm('Bu okulu silmek istiyor musunuz?')) {
      this.reportService.getTargets().subscribe((schoolData: {}) => {
        schoolData['response'].forEach(response => {
          if (response['name'] == event.data['name']) {
            console.log(schoolData);
            this.reportService.removeTarget(response['id']).subscribe((data: {}) => {
            });
          }
        });
      });
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onUserRowSelect(event): void {

    if (this.firstClick == 0) {
      this.id = event.data['name'];
      this.firstClick = new Date().getTime();
      this.controlClick = 1;
      this.secondClick = 0;
      this.reportService.getTargets().subscribe((schoolData: {}) => {
        schoolData['response'].forEach(response => {
          if (response['code'] && response['code'] == event.data['code']) {
            this.code = response['code'];
          }
        });
      });
    }
    if (this.firstClick != 0 && this.secondClick != 0 && ((new Date().getTime()) - this.firstClick) < 500 && (this.id == event.data['name'])) {
      this.reportService.getTargets().subscribe((schoolData: {}) => {
        schoolData['response'].forEach(response => {

          if (response['name'] && response['name'] == event.data['name']) {
            this.reportService.schoolData['schoolId'] = response['id'];
            this.reportService.schoolData['schoolName'] = response['name'];
            this.reportService.schoolData['schoolAddress'] = response['adrLocation']['address'];
            this.reportService.schoolData['schoolLatitude'] = response['adrLocation']['location']['latitude'];
            this.reportService.schoolData['schoolLongitude'] = response['adrLocation']['location']['longitude'];

          }
        });
        this.dialogService.open(SchoolEditComponent, {
          context: {
            title: 'Okul Bilgileri',
          },
        });
      });
      this.firstClick = 0;
      this.secondClick = 0;
    }
    else if (this.firstClick != 0) {
      this.reportService.getTargets().subscribe((schoolData: {}) => {
        schoolData['response'].forEach(response => {
          if (response['code'] && response['code'] == event.data['code']) {
            this.code = response['code'];
          }
        });
      });
      this.id = event.data['name'];
      this.firstClick = new Date().getTime();
      this.secondClick = 1;
    }
  }
  newSchool() {
    this.dialogService.open(SchoolNewComponent, {
      context: {
        title: 'Okul Bilgileri',
      },
    });
  }
  editSchool() {
    if (this.controlClick == 1) {
      this.reportService.getTargets().subscribe((schoolData: {}) => {
        schoolData['response'].forEach(response => {
          if (response['id'] && response['name'] == this.id) {
            console.log(response);
            this.reportService.schoolData['schoolId'] = response['id'];
            this.reportService.schoolData['schoolName'] = response['name'];
            this.reportService.schoolData['schoolAddress'] = response['adrLocation']['address'];
            this.reportService.schoolData['schoolLatitude'] = response['adrLocation']['location']['latitude'];
            this.reportService.schoolData['schoolLongitude'] = response['adrLocation']['location']['longitude'];
          }
        });
        this.dialogService.open(SchoolEditComponent, {
          context: {
            title: 'Okul Bilgileri',
          },
        });
      });
    }
  }
  ngOnInit() {
    var tableData = [];
    var location = [];
    this.reportService.getTargets().subscribe((schoolData: {}) => {
      schoolData['response'].forEach(response => {
        location = [];
        location.push(response['adrLocation']['location']['latitude']);
        location.push(response['adrLocation']['location']['longitude']);
        tableData.push({
          name: response['name'], address: response['adrLocation']['address'],
          location: location.join(" , ")
        });
      });
      this.source.load(tableData);
    });
  }
}

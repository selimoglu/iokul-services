import { Component, Input } from '@angular/core';
import { NbWindowRef, NbDialogRef, NbComponentSize, NbDialogService } from '@nebular/theme';
import { ReportData } from '../../../@core/interfaces/common/reportData';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { SchoolDialogComponent } from '../school-dialog/school-dialog.component';

@Component({
  templateUrl: './school-new.component.html',
  styleUrls: ['school-new.component.scss'],
})
export class SchoolNewComponent {

  schools = [];
  @Input() title: string;
  sizes: NbComponentSize[] = ['medium',];
  schoolForm = new FormGroup({
    name: new FormControl(''),
    address: new FormControl(''),
    latitude: new FormControl(''),
    longitude: new FormControl(''),
  });
  selectedLatitude = "";
  selectedLongitude = "";
  constructor(private reportService: ReportData, protected ref: NbDialogRef<SchoolNewComponent>, private dialogService: NbDialogService) {

  }

  save() {
    var id, name, address, latitude, longitude;
    name = this.schoolForm.value.name;
    address = this.schoolForm.value.address;
    latitude = this.schoolForm.value.latitude;
    longitude = this.schoolForm.value.longitude;
    if (name && address) {
      this.reportService.createTarget(id, name, address, latitude, longitude).subscribe((data: {}) => {
        this.ref.close();
        window.location.reload();
      });

    }
    else if (!name) {
      this.dialogService.open(SchoolDialogComponent, {
        context: {
          title: 'Okul adı',
        },
      });
    }
    else if (!address) {
      this.dialogService.open(SchoolDialogComponent, {
        context: {
          title: 'Okul adresi',
        },
      });
    }
  }

  close() {
    this.ref.close();
  }
  ngOnInit() {

  }

}

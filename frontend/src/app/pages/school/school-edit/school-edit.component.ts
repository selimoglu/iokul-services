import { Component, Input } from '@angular/core';
import { NbDialogRef, NbComponentSize, NbDialogService } from '@nebular/theme';
import { ReportData } from '../../../@core/interfaces/common/reportData';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { SchoolDialogComponent } from '../school-dialog/school-dialog.component';

@Component({
  templateUrl: './school-edit.component.html',
  styleUrls: ['school-edit.component.scss'],
})
export class SchoolEditComponent {
  schools = [];
  @Input() title: string;
  sizes: NbComponentSize[] = ['medium',];
  schoolForm = new FormGroup({
    name: new FormControl(''),
    address: new FormControl(''),
    latitude: new FormControl(''),
    longitude: new FormControl(''),
  });
  constructor(private reportService: ReportData, protected ref: NbDialogRef<SchoolEditComponent>, private formBuilder: FormBuilder, private dialogService: NbDialogService) {
  }

  save() {
    var id, name, address, latitude,longitude;
    id = this.reportService.schoolData['schoolId']
    name = this.schoolForm.value.name;
    address = this.schoolForm.value.address;
    latitude= this.schoolForm.value.latitude;
    longitude= this.schoolForm.value.longitude;
    if (name && address ) {
      this.reportService.createTarget(id, name, address, latitude,longitude).subscribe((data: {}) => {
        this.ref.close();
        window.location.reload();
      });
    }
    else if (!name) {
      this.dialogService.open(SchoolDialogComponent, {
        context: {
          title: 'Okul adı',
        },
      });
    }
    else if (!address) {
      this.dialogService.open(SchoolDialogComponent, {
        context: {
          title: 'Okul adresi',
        },
      });
    }
  }
  delete() {

    this.reportService.removeTarget(this.reportService.schoolData['schoolId']).subscribe((data: {}) => {
      this.ref.close();
      window.location.reload();
    });

  }
  close() {
    this.ref.close();
  }
  ngOnInit() {

    this.schoolForm = this.formBuilder.group({
      name: [this.reportService.schoolData['schoolName'], Validators.required],
      address: [this.reportService.schoolData['schoolAddress'], Validators.required],
      latitude: [this.reportService.schoolData['schoolLatitude'], Validators.required],
      longitude: [this.reportService.schoolData['schoolLongitude'], Validators.required],
    });
  }
}

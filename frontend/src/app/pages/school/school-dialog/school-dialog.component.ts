import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'school-dialog',
  templateUrl: 'school-dialog.component.html',
  styleUrls: ['school-dialog.component.scss'],
})
export class SchoolDialogComponent {

  @Input() title: string;

  constructor(protected ref: NbDialogRef<SchoolDialogComponent>) {}

  dismiss() {
    this.ref.close();
  }
}

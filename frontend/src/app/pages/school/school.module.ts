import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { SchoolReportTableComponent } from './school-report-table/school-report-table.component';
import { SchoolEditComponent } from './school-edit/school-edit.component';
import { SchoolNewComponent } from './school-new/school-new.component';
import { SchoolDialogComponent } from './school-dialog/school-dialog.component';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbTabsetModule,
  NbUserModule,
  NbRadioModule,
  NbSelectModule,
  NbListModule,
  NbIconModule,
  NbSpinnerModule,
  NbInputModule,
  NbDialogModule,
} from '@nebular/theme';

import { NgxEchartsModule } from 'ngx-echarts';
import { ThemeModule } from '../../@theme/theme.module';
import { SchoolComponent } from './school.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthModule } from '../../@auth/auth.module';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  imports: [
    FormsModule,
    ThemeModule,
    NbCardModule,
    NbUserModule,
    NbButtonModule,
    NbTabsetModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbListModule,
    NbIconModule,
    NbSpinnerModule,
    NbDialogModule.forChild(),
    NgxEchartsModule,
    AuthModule,
    Ng2SmartTableModule,
    NbInputModule,
    ReactiveFormsModule,
    AgmCoreModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB4CrPW8wExGyWLuluO4gEUBbGuhT2EXCw'
    }),
  ],
  declarations: [
    SchoolComponent,
    SchoolReportTableComponent,
    SchoolEditComponent,
    SchoolNewComponent,
    SchoolDialogComponent,
  ],
  entryComponents: [SchoolEditComponent,SchoolNewComponent,SchoolDialogComponent],
})
export class SchoolModule { }

/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */

import {Component, OnDestroy} from '@angular/core';
import { takeWhile } from 'rxjs/operators' ;
import { SchoolEditComponent } from './school-edit/school-edit.component';
import { NbWindowService, NbDialogService } from '@nebular/theme';

@Component({
  selector: 'ngx-school',
  styleUrls: ['./school.component.scss'],
  templateUrl: './school.component.html',
})
export class SchoolComponent implements OnDestroy {

  private alive = true;

  constructor(private dialogService: NbDialogService) {
  }

  open() {
    this.dialogService.open(SchoolEditComponent, {
      context: {
        title: 'Okul Bilgileri',
      },
    });
  }
  edit(){
    this.dialogService.open(SchoolEditComponent, {
      context: {
        title: 'Okul Bilgileri',
      },
    });
  }
  ngOnDestroy() {
    this.alive = false;
  }
}

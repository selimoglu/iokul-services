/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */

import { Component } from '@angular/core';
import { NbWindowRef } from '@nebular/theme';

@Component({
  templateUrl: './map-edit.component.html',
  styleUrls: ['map-edit.component.scss'],
})
export class MapEditComponent {
  constructor(public windowRef: NbWindowRef) { }

  close() {
    this.windowRef.close();
  }
}

/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { SmartTableData } from '../../../@core/interfaces/common/smart-table';
import { MapEditComponent } from '../map-edit/map-edit.component';
import { NbWindowService } from '@nebular/theme';

@Component({
  selector: 'map-report-table',
  templateUrl: './map-report-table.component.html',
  styleUrls: ['./map-report-table.component.scss'],
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class MapReportTableComponent {

  settings = {
    actions: {
      add: false,
      edit: false,
      delete: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'Harita',
        type: 'number',
      },
      firstName: {
        title: 'Servisçiler Listesi',
        type: 'string',
      },
    
    },
  };
  source: LocalDataSource = new LocalDataSource();

  constructor(private service: SmartTableData, private windowService: NbWindowService,) {
    const data = this.service.getData();
    this.source.load(data);
  }
  openWindowForm() {

    this.windowService.open(MapEditComponent, { title: `Harita Bilgileri` });

  }
  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}

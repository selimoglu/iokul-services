/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */

import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { MapReportTableComponent } from './map-report-table/map-report-table.component';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbTabsetModule,
  NbUserModule,
  NbRadioModule,
  NbSelectModule,
  NbListModule,
  NbIconModule,
  NbSpinnerModule,
  NbWindowModule,
} from '@nebular/theme';

import { NgxEchartsModule } from 'ngx-echarts';
import { ThemeModule } from '../../@theme/theme.module';
import { MapComponent } from './map.component';
import { FormsModule } from '@angular/forms';
import { AuthModule } from '../../@auth/auth.module';
import { MapEditComponent } from './map-edit/map-edit.component';

@NgModule({
  imports: [
    FormsModule,
    ThemeModule,
    NbCardModule,
    NbUserModule,
    NbButtonModule,
    NbTabsetModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbListModule,
    NbIconModule,
    NbButtonModule,
    NbSpinnerModule,
    NbWindowModule,
    NbWindowModule.forChild(),
    NgxEchartsModule,
    AuthModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    MapComponent,
    MapReportTableComponent,
    MapEditComponent
  ],
  entryComponents: [MapEditComponent],
})
export class MapModule { }

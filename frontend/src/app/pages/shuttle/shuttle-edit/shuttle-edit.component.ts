import { Component, Input } from '@angular/core';
import { NbDialogRef, NbComponentSize, NbDialogService, NbComponentStatus } from '@nebular/theme';
import { ReportData } from '../../../@core/interfaces/common/reportData';
import { Subscription } from 'rxjs';
import { MessageService } from '../../../@core/mock/common/message-service';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ShuttleDialogComponent } from '../shuttle-dialog/shuttle-dialog.component';

@Component({
  templateUrl: './shuttle-edit.component.html',
  styleUrls: ['shuttle-edit.component.scss'],
})

export class ShuttleEditComponent {
  @Input() title: string;

  shuttleForm = new FormGroup({
    name: new FormControl(''),
    phone: new FormControl(''),
    code: new FormControl(''),
    plate: new FormControl(''),
    mark: new FormControl(''),
    model: new FormControl(''),
    modelYear: new FormControl(''),
    capacity: new FormControl(''),
  });

  schools = [];
  selectedSchool = [];
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];
  subscription: Subscription;
  newArr = [];
  selectedCode = "";
  constructor(private reportService: ReportData, private formBuilder: FormBuilder,
    private messageService: MessageService,
    protected ref: NbDialogRef<ShuttleEditComponent>, private router: Router, private dialogService: NbDialogService) {
    this.selectedCode = this.reportService.shuttleData['shuttleCode'];
    this.schools = this.reportService.schoolArr;

    console.log(this.reportService.shuttleData);
    for (var i = 0; i < this.reportService.shuttleData['targetsIds'].length; i++) {
      this.newArr[i] = this.reportService.shuttleData['targetsIds'][i].id;
    }
    console.log(this.newArr);
    this.selectedSchool = this.newArr;

  }

  delete() {

    this.reportService.removeShuttle(this.reportService.shuttleData['shuttleId']).subscribe((data: {}) => {
      this.ref.close();
      window.location.reload();
    });
  }

  save() {
    var id, name, code, phone, plate, mark, model, modelYear, capacity;
    var targetsId = [];
    if (this.selectedSchool[0]) {
      targetsId = this.selectedSchool;
    }

    if (this.phoneToNumber(this.shuttleForm.value.phone).length == 12) {
      phone = this.phoneToNumber(this.shuttleForm.value.phone);
    }
    id = this.reportService.shuttleData['shuttleId'];
    name = this.shuttleForm.value.name;
    code =  this.selectedCode;
    plate = this.shuttleForm.value.plate;
    mark = this.shuttleForm.value.mark;
    model = this.shuttleForm.value.model;
    modelYear = this.shuttleForm.value.modelYear;
    capacity = this.shuttleForm.value.capacity;

    if (name && code && phone && plate) {
      this.reportService.createShuttle(id, name, code, phone, targetsId, plate, mark, model, modelYear, capacity).subscribe((data: {}) => {
        this.ref.close();
        window.location.reload();
      });

    }
    else if (!name) {
      this.dialogService.open(ShuttleDialogComponent, {
        context: {
          title: 'Sürücü adı',
        },
      });
    }
    else if (!phone) {
      this.dialogService.open(ShuttleDialogComponent, {
        context: {
          title: 'Cep telefonu',
        },
      });
    }
    else if (!targetsId[0]) {
      targetsId = [];
      this.dialogService.open(ShuttleDialogComponent, {
        context: {
          title: 'Okul adı',
        },
      });
    }
    else if (!code) {
      this.dialogService.open(ShuttleDialogComponent, {
        context: {
          title: 'Sürücü codu',
        },
      });
    }
    else if (!plate) {
      this.dialogService.open(ShuttleDialogComponent, {
        context: {
          title: 'Araç plakası',
        },
      });
    }
  }


  makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    this.selectedCode = result;
  }

  phoneToNumber(s: string) {
    s = s.replace('_', '');
    s = s.replace('(', '');
    s = s.replace(')', '');
    s = s.replace(' ', '');
    s = s.replace('-', '');
    s = s.replace('-', '');
    s = ("90" + s).slice(-17);
    return s;
  }

  close() {
    this.ref.close();
  }

  ngOnInit() {
    this.shuttleForm = this.formBuilder.group({
      name: [this.reportService.shuttleData['shuttleName'], Validators.required],
      phone: [this.reportService.shuttleData['shuttlePhone'], Validators.required],
      code: [this.reportService.shuttleData['shuttleCode'], Validators.required],
      plate: [this.reportService.shuttleData['plate'], Validators.required],
      mark: [this.reportService.shuttleData['mark'], Validators.required],
      model: [this.reportService.shuttleData['model'], Validators.required],
      modelYear: [this.reportService.shuttleData['modelYear'], Validators.required],
      capacity: [this.reportService.shuttleData['capacity'], Validators.required],
    });

  }


}

/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Personal / Commercial License.
 * See LICENSE_PERSONAL / LICENSE_COMMERCIAL in the project root for license information on type of purchased license.
 */

import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/interfaces/common/smart-table';
import { ReportData } from '../../../@core/interfaces/common/reportData';
import { NbDialogService } from '@nebular/theme';
import { ShuttleEditComponent } from '../shuttle-edit/shuttle-edit.component';
import { ShuttleNewComponent } from '../shuttle-new/shuttle-new.component';
import { MessageService } from '../../../@core/mock/common/message-service';
@Component({
  selector: 'shuttle-report-table',
  templateUrl: './shuttle-report-table.component.html',
  styleUrls: ['./shuttle-report-table.component.scss'],
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class ShuttleReportTableComponent {

  settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      name: {
        title: 'Sürücü Adı',
        type: 'string',
      },
      plate: {
        title: 'Araç Plakası',
        type: 'string',
      },
      phone: {
        title: 'Cep Telefonu',
        type: 'string',
      },
      code: {
        title: 'Kod',
        type: 'string',
      },
      schools: {
        title: 'Okullar',
        type: 'string',
      },
    },
  };
  firstClickRow: string;
  id: string;
  sidebarState: boolean;
  code: string;
  source: LocalDataSource = new LocalDataSource();
  controlClick: number;
  sendSchools = [];
  firstClick = 0;
  secondClick = 0;
  typeTexts = { text: 'Today', value: 'today' };
  constructor(private service: SmartTableData, private reportService: ReportData, private dialogService: NbDialogService, private messageService: MessageService) {

  }

  onDeleteConfirm(event): void {

    if (window.confirm('Bu servisi silmek istiyor musunuz?')) {
      this.reportService.getShuttle().subscribe((shuttleData: {}) => {
        shuttleData['response'].forEach(response => {
          if (response['code'] == event.data['code']) {
            this.reportService.removeShuttle(response['id']).subscribe((data: {}) => {
            });
          }
        });
      });
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  onUserRowSelect(event): void {
    if (this.firstClick == 0) {
      this.id = event.data['name'];
      this.firstClick = new Date().getTime();
      this.controlClick = 1;
      this.secondClick = 0;
      this.reportService.getShuttle().subscribe((shuttleData: {}) => {
        shuttleData['response'].forEach(response => {
          if (response['code'] && response['code'] == event.data['code']) {
            this.code = response['code'];
          }
        });
      });
    }
    if (this.firstClick != 0 && this.secondClick != 0 && ((new Date().getTime()) - this.firstClick) < 500 && (this.id == event.data['name'])) {

      this.reportService.getShuttle().subscribe((shuttleData: {}) => {
        shuttleData['response'].forEach(response => {
          if (response['code'] && response['code'] == event.data['code']) {
            this.reportService.shuttleData['shuttleId'] = response['id'];
            this.reportService.shuttleData['shuttleName'] = response['name'];
            this.reportService.shuttleData['shuttlePhone'] = (response['phone'] + "").substring(2);
            this.reportService.shuttleData['shuttleCode'] = response['code'];
            this.reportService.shuttleData['targetsIds'] = response['targets'];
            this.reportService.shuttleData['plate'] = response['car']['plate'];
            this.reportService.shuttleData['mark'] = response['car']['mark'];
            this.reportService.shuttleData['model'] = response['car']['model'];
            this.reportService.shuttleData['modelYear'] = response['car']['modelYear'];
            this.reportService.shuttleData['capacity'] = response['car']['capacity'];
          }
        });

        this.dialogService.open(ShuttleEditComponent, {
          context: {
            title: 'Servis Bilgileri',
          },
        });
      });
      this.firstClick = 0;
      this.secondClick = 0;
    }
    else if (this.firstClick != 0) {
      this.reportService.getShuttle().subscribe((shuttleData: {}) => {
        shuttleData['response'].forEach(response => {
          if (response['code'] && response['code'] == event.data['code']) {
            this.code = response['code'];
          }
        });
      });
      this.id = event.data['name'];
      this.firstClick = new Date().getTime();
      this.secondClick = 1;
    }
  }
  newShuttle() {
    this.dialogService.open(ShuttleNewComponent, {
      context: {
        title: 'Araç Bilgileri',
      },
    });
  }
  editShuttle() {
    this.reportService.schoolData['schoolArray'] = this.sendSchools;
    if (this.controlClick == 1) {
      this.reportService.getShuttle().subscribe((shuttleData: {}) => {

        shuttleData['response'].forEach(response => {

          if (response['code'] && response['code'] == this.code) {
            this.reportService.shuttleData['shuttleId'] = response['id'];
            this.reportService.shuttleData['shuttleName'] = response['name'];
            this.reportService.shuttleData['shuttlePhone'] = (response['phone'] + "").substring(2);
            this.reportService.shuttleData['shuttleCode'] = response['code'];
            this.reportService.shuttleData['targetsIds'] = response['targets'];
            this.reportService.shuttleData['plate'] = response['car']['plate'];
            this.reportService.shuttleData['mark'] = response['car']['mark'];
            this.reportService.shuttleData['model'] = response['car']['model'];
            this.reportService.shuttleData['modelYear'] = response['car']['modelYear'];
            this.reportService.shuttleData['capacity'] = response['car']['capacity'];
          }
        });

        this.dialogService.open(ShuttleEditComponent, {
          context: {
            title: 'Servis Bilgileri',
          },
        });
      });
    }
  }

  transformPhone(val) {

    let newStr = '';
    newStr = newStr + val.substr(0, 3) + '-';
    newStr = newStr + val.substr(3, 3) + '-';
    newStr = newStr + val.substr(6, 2) + '-';
    return newStr + val.substr(8, 2);

  }


  ngOnInit() {
    this.reportService.getTargets().subscribe((schoolData: {}) => {
      schoolData['response'].forEach(response => {

        this.sendSchools.push({
          id: response['id'], name: response['name']
        });
      });
      this.reportService.schoolArr = this.sendSchools;
    });

    const tableData = [];
    var name, plate, phone, code;
    var schools = [];
    this.reportService.getShuttle().subscribe((shuttleData: {}) => {
      shuttleData['response'].forEach(response => {
        schools = [];
        name = response['name'];
        plate = response['car']['plate'];
        phone = this.transformPhone((response['phone'] + "").substring(2));
        code = response['code']
        response['targets'].forEach(targets => {
          schools.push(targets['name']);
        });
        tableData.push({
          name: name, plate: plate,
          phone: phone, code: code, schools: schools.join(", ")
        });
      });
      this.source.load(tableData);
    });
  }
}

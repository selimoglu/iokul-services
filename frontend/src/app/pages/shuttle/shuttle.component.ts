import { Component, OnDestroy } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { ShuttleNewComponent } from './shuttle-new/shuttle-new.component';
import { NbWindowService, NbDialogService } from '@nebular/theme';

@Component({
  selector: 'ngx-shuttle',
  styleUrls: ['./shuttle.component.scss'],
  templateUrl: './shuttle.component.html',
})
export class ShuttleComponent implements OnDestroy {

  private alive = true;
  constructor(private dialogService: NbDialogService ) {
  }
  ngOnDestroy() {
    this.alive = false;
  }
}

import { Component, Input, TemplateRef } from '@angular/core';
import { NbDialogRef, NbComponentSize, NbDialogService } from '@nebular/theme';
import { ReportData } from '../../../@core/interfaces/common/reportData';
import { MessageService } from '../../../@core/mock/common/message-service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ShuttleDialogComponent } from '../shuttle-dialog/shuttle-dialog.component';
import { NumberFormatStyle } from '@angular/common';

@Component({
  templateUrl: './shuttle-new.component.html',
  styleUrls: ['shuttle-new.component.scss'],
})
export class ShuttleNewComponent {
  @Input() title: string;
  schools = [];
  shuttleForm = new FormGroup({
    name: new FormControl(''),
    phone: new FormControl(''),
    plate: new FormControl(''),
    mark: new FormControl(''),
    model: new FormControl(''),
    modelYear: new FormControl(''),
    capacity: new FormControl(''),
  });
  selectedSchool = new FormControl();
  selectedCode = "";
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];
  constructor(private reportService: ReportData, private formBuilder: FormBuilder,
    private messageService: MessageService,
    protected ref: NbDialogRef<ShuttleNewComponent>, private dialogService: NbDialogService) {
    this.makeid(6);
  }

  save() {
    var id, name, code, phone, plate, mark, model, modelYear, capacity;
    var targetIds = [];
    name = this.shuttleForm.value.name;
    code = this.selectedCode;

    if (this.phoneToNumber(this.shuttleForm.value.phone).length == 12) {
      phone = this.phoneToNumber(this.shuttleForm.value.phone);
    }

    plate = this.shuttleForm.value.plate;
    mark = this.shuttleForm.value.mark;
    model = this.shuttleForm.value.model;
    modelYear = this.shuttleForm.value.modelYear;
    capacity = this.shuttleForm.value.capacity;
    if (this.selectedSchool['value']) {
      this.selectedSchool['value'].forEach(value => {
        targetIds.push(value);
      });
    }
    console.log(targetIds);
    if (name && code && phone && plate && targetIds[0]) {
      this.reportService.createShuttle(id, name, code, phone, targetIds, plate, mark, model, modelYear, capacity).subscribe((data: {}) => {
        this.ref.close();
        window.location.reload();
      });
    }
    else if (!name) {
      targetIds = [];
      this.dialogService.open(ShuttleDialogComponent, {
        context: {
          title: 'Sürücü adı',
        },
      });
    }
    else if (!phone) {
      targetIds = [];
      this.dialogService.open(ShuttleDialogComponent, {
        context: {
          title: 'Cep telefonu',
        },
      });
    }
    else if (!targetIds[0]) {
      targetIds = [];
      this.dialogService.open(ShuttleDialogComponent, {
        context: {
          title: 'Okul adı',
        },
      });
    }
    else if (!code) {
      targetIds = [];
      this.dialogService.open(ShuttleDialogComponent, {
        context: {
          title: 'Sürücü codu',
        },
      });
    }
    else if (!plate) {
      targetIds = [];
      this.dialogService.open(ShuttleDialogComponent, {
        context: {
          title: 'Araç plakası',
        },
      });
    }
  }

  phoneToNumber(s: string) {
    s = s.replace('_', '');
    s = s.replace('(', '');
    s = s.replace(')', '');
    s = s.replace(' ', '');
    s = s.replace('-', '');
    s = s.replace('-', '');
    s = ("90" + s).slice(-17);
    return s;
  }

  makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    this.selectedCode = result;
  }


  close() {
  
    this.ref.close();


  }
  ngOnInit() {
    this.reportService.getTargets().subscribe((schoolData: {}) => {
      schoolData['response'].forEach(response => {
        this.schools.push({
          name: response['name'], id: response['id']
        });
      });
    });
  }
}

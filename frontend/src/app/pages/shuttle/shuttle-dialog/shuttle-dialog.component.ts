import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'shuttle-dialog',
  templateUrl: 'shuttle-dialog.component.html',
  styleUrls: ['shuttle-dialog.component.scss'],
})
export class ShuttleDialogComponent {

  @Input() title: string;

  constructor(protected ref: NbDialogRef<ShuttleDialogComponent>) {}

  dismiss() {
    this.ref.close();
  }
}

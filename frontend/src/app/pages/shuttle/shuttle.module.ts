import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ShuttleComponent } from './shuttle.component';
import { ShuttleReportTableComponent } from './shuttle-report-table/shuttle-report-table.component';
import { ShuttleEditComponent } from './shuttle-edit/shuttle-edit.component';
import { ShuttleNewComponent } from './shuttle-new/shuttle-new.component';
import { ShuttleDialogComponent } from './shuttle-dialog/shuttle-dialog.component';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbTabsetModule,
  NbUserModule,
  NbRadioModule,
  NbSelectModule,
  NbListModule,
  NbIconModule,
  NbSpinnerModule,
  NbInputModule,
  NbDialogModule,
} from '@nebular/theme';

import { NgxEchartsModule } from 'ngx-echarts';
import { ThemeModule } from '../../@theme/theme.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthModule } from '../../@auth/auth.module';
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
  imports: [
    FormsModule,
    ThemeModule,
    NbCardModule,
    NbUserModule,
    NbButtonModule,
    NbTabsetModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbListModule,
    NbIconModule,
    NbButtonModule,
    NbSpinnerModule,
    NbDialogModule.forChild(),
    NgxEchartsModule,
    AuthModule,
    Ng2SmartTableModule,
    NbInputModule,
    ReactiveFormsModule,
    TextMaskModule,
  ],
  declarations: [
    ShuttleComponent,
    ShuttleReportTableComponent,
    ShuttleEditComponent,
    ShuttleNewComponent,
    ShuttleDialogComponent,
  ],
  entryComponents: [ShuttleEditComponent,ShuttleNewComponent,ShuttleDialogComponent],
})

export class ShuttleModule { }

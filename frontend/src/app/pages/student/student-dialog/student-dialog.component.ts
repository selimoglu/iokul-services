import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'student-dialog',
  templateUrl: 'student-dialog.component.html',
  styleUrls: ['student-dialog.component.scss'],
})
export class StudentDialogComponent {

  @Input() title: string;

  constructor(protected ref: NbDialogRef<StudentDialogComponent>) {}

  dismiss() {
    this.ref.close();
  }
}

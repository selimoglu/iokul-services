import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { StudentReportTableComponent } from './student-report-table/student-report-table.component';
import { StudentEditComponent } from './student-edit/student-edit.component';
import { StudentNewComponent } from './student-new/student-new.component';
import { StudentDialogComponent } from './student-dialog/student-dialog.component';

import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbTabsetModule,
  NbUserModule,
  NbRadioModule,
  NbSelectModule,
  NbListModule,
  NbIconModule,
  NbSpinnerModule,
  NbInputModule,
  NbDialogModule,
} from '@nebular/theme';

import { NgxEchartsModule } from 'ngx-echarts';
import { ThemeModule } from '../../@theme/theme.module';
import { StudentComponent } from './student.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthModule } from '../../@auth/auth.module';
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
  imports: [
    FormsModule,
    ThemeModule,
    NbCardModule,
    NbUserModule,
    NbButtonModule,
    NbTabsetModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbListModule,
    NbIconModule,
    NbButtonModule,
    NbSpinnerModule,
    NbDialogModule.forChild(),
    NgxEchartsModule,
    AuthModule,
    Ng2SmartTableModule,
    NbInputModule,
    ReactiveFormsModule,
    TextMaskModule,
  ],
  declarations: [
    StudentComponent,
    StudentReportTableComponent,
    StudentEditComponent,
    StudentNewComponent,
    StudentDialogComponent
  ],
  entryComponents: [StudentEditComponent,StudentNewComponent,StudentDialogComponent],
})
export class StudentModule { }

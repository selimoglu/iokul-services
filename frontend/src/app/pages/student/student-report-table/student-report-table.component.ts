import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { ReportData } from '../../../@core/interfaces/common/reportData';
import { Subscription } from 'rxjs';
import { StudentNewComponent } from '../student-new/student-new.component';
import { NbDialogService } from '@nebular/theme';
import { StudentEditComponent } from '../student-edit/student-edit.component';
@Component({
  selector: 'student-report-table',
  templateUrl: './student-report-table.component.html',
  styleUrls: ['./student-report-table.component.scss'],
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class StudentReportTableComponent {

  settings = {
    actions: {
      add: false,
      edit: false,
      delete: false,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      name: {
        title: 'Öğrenci Adı',
        type: 'string',
      },
      code: {
        title: 'Öğrenci Kodu',
        type: 'string',
      },
      schoolName: {
        title: 'Okul Adı',
        type: 'string',
      },
      shuttleName: {
        title: 'Servis Aracı',
        type: 'string',
      },
      parentName: {
        title: 'Veli Adı',
        type: 'string',
      },
      parentPhone: {
        title: 'Veli Telefon ',
        type: 'string',
      },
      parentCode: {
        title: 'Veli Kodu',
        type: 'string',
      },
    },
  };
  firstClickRow: string;
  id: string;
  sidebarState: boolean;
  code: string;
  source: LocalDataSource = new LocalDataSource();
  controlClick: number;
  firstClick = 0;
  secondClick = 0;
  sendSchools = [];
  sendShuttle = [];
  constructor(private reportService: ReportData, private dialogService: NbDialogService) {

  }
  onUserRowSelect(event): void {
    if (this.firstClick == 0) {
      this.id = event.data['name'];
      this.firstClick = new Date().getTime();
      this.controlClick = 1;
      this.secondClick = 0;
      this.reportService.getStudents().subscribe((studentData: {}) => {
        studentData['response'].forEach(response => {
          if (response['code'] && response['code'] == event.data['code']) {
            this.code = response['code'];
          }
        });
      });
    }

    if (this.firstClick != 0 && this.secondClick != 0 && ((new Date().getTime()) - this.firstClick) < 500 && (this.id == event.data['name'])) {
      this.reportService.getStudents().subscribe((studentData: {}) => {
        studentData['response'].forEach(response => {
          if (response['code'] && response['code'] == event.data['code']) {
            this.reportService.studentData['address'] = response['adrLocation']['address'];
            this.reportService.studentData['studentCode'] = response['code'];
            this.reportService.studentData['studentId'] = response['id'];
            this.reportService.studentData['studentName'] = response['name'];
            this.reportService.studentData['parentCode'] = response['parentCode'];
            this.reportService.studentData['parentName'] = response['parentName'];
            this.reportService.studentData['parentPhone'] = (response['parentPhone'] + "").substring(2);
            this.reportService.studentData['shuttleName'] = response['shuttleName'];
            this.reportService.studentData['shuttleId'] = response['shuttleId'];
            this.reportService.studentData['targetName'] = response['targetName'];
            this.reportService.studentData['targetId'] = response['targetId'];
            this.reportService.studentData['studentLatitude'] = response['adrLocation']['location']['latitude'];
            this.reportService.studentData['studentLongitude'] = response['adrLocation']['location']['longitude'];
          }
        });
        this.dialogService.open(StudentEditComponent, {
          context: {
            title: 'Öğrenci bilgileri',
          },
        });
      });
      this.firstClick = 0;
      this.secondClick = 0;
    }
    else if (this.firstClick != 0) {
      this.reportService.getStudents().subscribe((studentData: {}) => {
        studentData['response'].forEach(response => {
          if (response['code'] && response['code'] == event.data['code']) {
            this.code = response['code'];
          }
        });
      });
      this.id = event.data['name'];
      this.firstClick = new Date().getTime();
      this.secondClick = 1;
    }
  }
  onDeleteConfirm(event): void {

    if (window.confirm('Bu öğrenciyi silmek istiyor musunuz?')) {
      this.reportService.getStudents().subscribe((studentsData: {}) => {
        studentsData['response'].forEach(response => {
          if (response['name'] == event.data['name']) {
            console.log(studentsData);
            this.reportService.removeStudent(response['id']).subscribe((data: {}) => {
            });
          }
        });
      });
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
  newStudent() {
    this.dialogService.open(StudentNewComponent, {
      context: {
        title: 'Öğrenci Bilgileri',
      },
    });
  }

  editStudent() {

    if (this.controlClick == 1) {
      this.reportService.getStudents().subscribe((studentData: {}) => {
        console.log(studentData);
        studentData['response'].forEach(response => {
          if (response['code'] && response['code'] == this.code) {
            this.reportService.studentData['address'] = response['adrLocation']['address'];
            this.reportService.studentData['studentCode'] = response['code'];
            this.reportService.studentData['studentId'] = response['id'];
            this.reportService.studentData['studentName'] = response['name'];
            this.reportService.studentData['parentCode'] = response['parentCode'];
            this.reportService.studentData['parentName'] = response['parentName'];
            this.reportService.studentData['parentPhone'] = (response['parentPhone'] + "").substring(2);
            this.reportService.studentData['shuttleName'] = response['shuttleName'];
            this.reportService.studentData['shuttleId'] = response['shuttleId'];
            this.reportService.studentData['targetName'] = response['targetName'];
            this.reportService.studentData['targetId'] = response['targetId'];
            this.reportService.studentData['studentLatitude'] = response['adrLocation']['location']['latitude'];
            this.reportService.studentData['studentLongitude'] = response['adrLocation']['location']['longitude'];
          }
        });
        this.dialogService.open(StudentEditComponent, {
          context: {
            title: 'Öğrenci bilgileri',
          },
        });
      });
    }
  }

  transformPhone(val) {

    let newStr = '';
    newStr = newStr + val.substr(0, 3) + '-';
    newStr = newStr + val.substr(3, 3) + '-';
    newStr = newStr + val.substr(6, 2) + '-';
    return newStr + val.substr(8, 2);

  }

  ngOnInit() {

    const tableData = [];
    this.reportService.getTargets().subscribe((schoolData: {}) => {
      schoolData['response'].forEach(response => {

        this.sendSchools.push({
          id: response['id'], name: response['name']
        });
      });
      this.reportService.schoolArr = this.sendSchools;
    });
    this.reportService.getShuttle().subscribe((shuttleData: {}) => {
      this.sendShuttle = [];
      shuttleData['response'].forEach(response => {

        this.sendShuttle.push({
          id: response['id'], name: response['name']
        });
      });
      this.reportService.shuttleArr = this.sendShuttle;
    });

    this.reportService.getStudents().subscribe((studentData: {}) => {
      console.log(studentData);
      studentData['response'].forEach(response => {
        var x = response['parentPhone'];
        if (response['parentPhone']) {
          x = this.transformPhone((response['parentPhone'] + "").substring(2))
        }
        tableData.push({
          name: response['name'], code: response['code'], schoolName: response['targetName'], shuttleName: response['shuttleName'],
          parentName: response['parentName'], parentPhone: x, parentCode: response['parentCode'],
        });
      });
      this.source.load(tableData);
    });
  }
}

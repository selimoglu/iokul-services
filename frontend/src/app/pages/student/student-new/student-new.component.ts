import { Component, Input, TemplateRef } from '@angular/core';
import { NbDialogRef, NbComponentSize, NbDialogService } from '@nebular/theme';
import { ReportData } from '../../../@core/interfaces/common/reportData';
import { MessageService } from '../../../@core/mock/common/message-service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { StudentDialogComponent } from '../student-dialog/student-dialog.component';

@Component({
  templateUrl: './student-new.component.html',
  styleUrls: ['student-new.component.scss'],
})
export class StudentNewComponent {
  @Input() title: string;
  schools = [];
  shuttles = [];
  studentForm = new FormGroup({
    studentName: new FormControl(''),
    studentCode: new FormControl(''),
    address: new FormControl(''),
    parentName: new FormControl(''),
    parentPhone: new FormControl(''),
    parentCode: new FormControl(''),
    studentLatitude: new FormControl(''),
    studentLongitude: new FormControl(''),
  });
  selectedShuttle = new FormControl();
  selectedSchool = new FormControl();
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];
  selectedStudentCode = "";
  selectedParentCode = "";
  student = "student";
  parent = "parent";

  constructor(private reportService: ReportData, private formBuilder: FormBuilder,
    private messageService: MessageService,
    protected ref: NbDialogRef<StudentNewComponent>, private dialogService: NbDialogService) {
    this.makeid(6, this.student);
    this.makeid(6, this.parent);
  }

  save() {

    var studentId, studentName, studentCode, address, parentName, parentCode, parentPhone, shuttleId, targetId, studentLatitude, studentLongitude;
    studentName = this.studentForm.value.studentName;
    studentCode = this.selectedStudentCode;
    address = this.studentForm.value.address;
    parentName = this.studentForm.value.parentName;
    studentLatitude = this.studentForm.value.studentLatitude;
    studentLongitude = this.studentForm.value.studentLongitude;
    if (this.phoneToNumber(this.studentForm.value.parentPhone).length == 12) {
      parentPhone = this.phoneToNumber(this.studentForm.value.parentPhone);
    }
    parentCode = this.selectedParentCode;

    if (this.selectedShuttle['value']) {
      shuttleId = this.selectedShuttle['value'];
      console.log(shuttleId);
    }

    if (this.selectedSchool['value']) {
      targetId = this.selectedSchool['value'];
    }
    if (studentName && studentCode && address && parentCode && shuttleId && targetId) {
      this.reportService.createStudent(address, studentCode, studentId, studentName, parentCode, parentName, parentPhone, shuttleId, targetId, studentLatitude, studentLongitude).subscribe((data: {}) => {
        this.ref.close();
        window.location.reload();
      });
    }

    else if (!studentName) {
      this.dialogService.open(StudentDialogComponent, {
        context: {
          title: 'Öğrenci adı',
        },
      });
    }
    else if (!studentCode) {
      this.dialogService.open(StudentDialogComponent, {
        context: {
          title: 'Öğrenci kodu',
        },
      });
    }
    else if (!address) {
      this.dialogService.open(StudentDialogComponent, {
        context: {
          title: 'Adres',
        },
      });
    }
    else if (!targetId) {
      this.dialogService.open(StudentDialogComponent, {
        context: {
          title: 'Okul adı',
        },
      });
    }
    else if (!shuttleId) {
      this.dialogService.open(StudentDialogComponent, {
        context: {
          title: 'Servis',
        },
      });
    }

    else if (!parentCode) {
      this.dialogService.open(StudentDialogComponent, {
        context: {
          title: 'Veli kodu',
        },
      });
    }
  }

  close() {
    this.ref.close();
  }

  phoneToNumber(s: string) {
    s = s.replace('_', '');
    s = s.replace('(', '');
    s = s.replace(')', '');
    s = s.replace(' ', '');
    s = s.replace('-', '');
    s = s.replace('-', '');
    s = ("90" + s).slice(-17);
    return s;
  }
  makeid(length, person) {
    if (person == "student") {
      var result = '';
      var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var charactersLength = characters.length;
      for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }
      this.selectedStudentCode = result;
    }
    if (person == "parent") {
      var result = '';
      var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var charactersLength = characters.length;
      for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }
      this.selectedParentCode = result;
    }

  }


  ngOnInit() {
    this.reportService.getTargets().subscribe((schoolData: {}) => {
      console.log(schoolData);
      schoolData['response'].forEach(response => {
        this.schools.push({
          name: response['name'], id: response['id']
        });
      });
    });
    this.reportService.getShuttle().subscribe((shuttleData: {}) => {
      console.log(shuttleData);
      shuttleData['response'].forEach(response => {
        this.shuttles.push({
          name: response['name'], id: response['id']
        });
      });
    });
  }
}

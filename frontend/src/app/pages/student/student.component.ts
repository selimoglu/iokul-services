import { Component, OnDestroy } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { StudentNewComponent } from './student-new/student-new.component';
import { NbWindowService, NbDialogService } from '@nebular/theme';

@Component({
  selector: 'ngx-student',
  styleUrls: ['./student.component.scss'],
  templateUrl: './student.component.html',
})
export class StudentComponent implements OnDestroy {

  private alive = true;
  constructor(private dialogService: NbDialogService ) {
  }
  ngOnDestroy() {
    this.alive = false;
  }
}

import { Component, Input } from '@angular/core';
import { NbWindowRef, NbDialogRef, NbDialogService } from '@nebular/theme';
import { ReportData } from '../../../@core/interfaces/common/reportData';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { StudentDialogComponent } from '../student-dialog/student-dialog.component';

@Component({
  templateUrl: './student-edit.component.html',
  styleUrls: ['student-edit.component.scss'],
})
export class StudentEditComponent {

  @Input() title: string;
  studentForm = new FormGroup({
    studentName: new FormControl(''),
    studentCode: new FormControl(''),
    address: new FormControl(''),
    parentName: new FormControl(''),
    parentPhone: new FormControl(''),
    parentCode: new FormControl(''),
    studentLatitude: new FormControl(''),
    studentLongitude: new FormControl(''),
  });
  schools = [];
  shuttles = [];
  selectedSchool = [];
  selectedShuttle = [];
  newArr = [];
  studentId: string;
  selectedStudentCode = "";
  selectedParentCode = "";
  student = "student";
  parent = "parent";
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/];
  constructor(private reportService: ReportData, protected ref: NbDialogRef<StudentEditComponent>, private formBuilder: FormBuilder, private dialogService: NbDialogService) {

  }
  save() {

    var studentName, studentCode, address, parentName, parentCode, parentPhone, shuttleId, targetId, studentLatitude, studentLongitude;

    studentName = this.studentForm.value.studentName;
    studentCode = this.selectedStudentCode
    address = this.studentForm.value.address;
    parentName = this.studentForm.value.parentName;
    studentLatitude = this.studentForm.value.studentLatitude;
    studentLongitude = this.studentForm.value.studentLongitude;
    if (this.phoneToNumber(this.studentForm.value.parentPhone).length == 12) {
      parentPhone = this.phoneToNumber(this.studentForm.value.parentPhone);
    }
    parentCode = this.selectedParentCode;
    targetId = this.selectedSchool;
    shuttleId = this.selectedShuttle;
    if (studentName && studentCode && address && parentCode) {
      this.reportService.createStudent(address, studentCode, this.studentId, studentName, parentCode, parentName, parentPhone, shuttleId, targetId, studentLatitude, studentLongitude).subscribe((data: {}) => {
        this.ref.close();
        window.location.reload();
      });
    }

    else if (!studentName) {
      this.dialogService.open(StudentDialogComponent, {
        context: {
          title: 'Öğrenci adı',
        },
      });
    }
    else if (!studentCode) {
      this.dialogService.open(StudentDialogComponent, {
        context: {
          title: 'Öğrenci kodu',
        },
      });
    }
    else if (!address) {
      this.dialogService.open(StudentDialogComponent, {
        context: {
          title: 'Adres',
        },
      });
    }
    else if (!targetId) {
      this.dialogService.open(StudentDialogComponent, {
        context: {
          title: 'Okul adı',
        },
      });
    }
    else if (!shuttleId) {
      this.dialogService.open(StudentDialogComponent, {
        context: {
          title: 'Servis',
        },
      });
    }

    else if (!parentCode) {
      this.dialogService.open(StudentDialogComponent, {
        context: {
          title: 'Veli kodu',
        },
      });
    }
  }
  delete() {

    this.reportService.removeStudent(this.reportService.studentData['studentId']).subscribe((data: {}) => {
      this.ref.close();
      window.location.reload();
    });

  }

  phoneToNumber(s: string) {
    s = s.replace('_', '');
    s = s.replace('(', '');
    s = s.replace(')', '');
    s = s.replace(' ', '');
    s = s.replace('-', '');
    s = s.replace('-', '');
    s = ("90" + s).slice(-17);
    return s;
  }

  close() {
    this.ref.close();
  }

  makeid(length, person) {
    if (person == "student") {
      var result = '';
      var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var charactersLength = characters.length;
      for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }
      this.selectedStudentCode = result;
    }
    if (person == "parent") {
      var result = '';
      var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var charactersLength = characters.length;
      for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }
      this.selectedParentCode = result;
    }

  }

  ngOnInit() {

    this.studentId = this.reportService.studentData['studentId'];
    this.selectedStudentCode = this.reportService.studentData['studentCode'];
    this.selectedParentCode = this.reportService.studentData['parentCode'];
    this.schools = this.reportService.schoolArr;
    this.shuttles = this.reportService.shuttleArr;
    this.selectedSchool = this.reportService.studentData['targetId'];
    this.selectedShuttle = this.reportService.studentData['shuttleId'];

    this.studentForm = this.formBuilder.group({
      studentName: [this.reportService.studentData['studentName'], Validators.required],
      studentCode: [this.reportService.studentData['studentCode'], Validators.required],
      address: [this.reportService.studentData['address'], Validators.required],
      parentName: [this.reportService.studentData['parentName'], Validators.required],
      parentPhone: [this.reportService.studentData['parentPhone'], Validators.required],
      parentCode: [this.reportService.studentData['parentCode'], Validators.required],
      studentLatitude: [this.reportService.studentData['studentLatitude'], Validators.required],
      studentLongitude: [this.reportService.studentData['studentLongitude'], Validators.required],
    });

  }
}

/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */

import { NbMenuItem } from '@nebular/theme';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class PagesMenu {

  getMenu(): Observable<NbMenuItem[]> {
    const general: NbMenuItem[] = [
      {
        title: 'GENEL',
        group: true,
      },
      {
        title: 'Okullar',
        icon: 'home-outline',
        link: '/pages/school',
        home: true,
        children: undefined,
      },
      {
        title: 'Araçlar',
        icon: 'car-outline',
        link: '/pages/shuttle',
        children: undefined,
      },
      {
        title: 'Öğrenciler',
        icon: 'person-outline',
        link: '/pages/student',
        children: undefined,
      },
    ];
    const map: NbMenuItem[] = [
      {
        title: 'HARİTA',
        group: true,
      },
    {
      title: 'Harita',
      icon: 'map-outline',
      link: '/pages/map',
      children: undefined,
    },
  ];
    return of([...general,...map]);
  }
}

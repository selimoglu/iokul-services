/*
 * Copyright (c) Akveo 2019. All Rights Reserved.
 * Licensed under the Single Application / Multi Application License.
 * See LICENSE_SINGLE_APP / LICENSE_MULTI_APP in the 'docs' folder for license information on type of purchased license.
 */

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { ShuttleComponent } from './shuttle/shuttle.component';
import { SchoolComponent } from './school/school.component';
import { StudentComponent } from './student/student.component';
import { MapComponent } from './map/map.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'school',
      component: SchoolComponent,
    },
    {
      path: 'shuttle',
      component: ShuttleComponent,
    },
    {
      path: 'student',
      component: StudentComponent,
    },
    {
      path: 'map',
      component: MapComponent,
    },
    {
      path: '',
      redirectTo: 'school',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
